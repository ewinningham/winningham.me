#!/usr/bin/env bash

JQ_VERSION='1.7.1'

mkdir -p ./tmp
curl -sL https://github.com/jqlang/jq/releases/download/jq-${JQ_VERSION}/jq-linux-amd64 > ./tmp/jq-linux-amd64
mv ./tmp/jq-linux-amd64 ./jq
chmod +x ./jq

