### README.md
This repo will be a fumbling attempt at trying to note and/or automate my personal domain, **winningham.me**.

## resources
* <https://github.com/opentofu/registry/tree/main/providers>
* <https://blog.gitguardian.com/a-comprehensive-guide-to-sops/>
* <https://gitlab.com/components/opentofu>
* <https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html>
* <https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html>
* <https://docs.gitlab.com/ee/user/project/merge_requests/auto_merge.html#require-a-successful-pipeline-for-merge>
