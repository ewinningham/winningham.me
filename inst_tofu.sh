#!/usr/bin/env bash

TOFU_VERSION='1.7.0'

mkdir -p ./tmp

curl -sL https://github.com/opentofu/opentofu/releases/download/v${TOFU_VERSION}/tofu_${TOFU_VERSION}_linux_amd64.zip > ./tmp/tofu_${TOFU_VERSION}_linux_amd64.zip
curl -sL https://github.com/opentofu/opentofu/releases/download/v${TOFU_VERSION}/tofu_${TOFU_VERSION}_SHA256SUMS > ./tmp/tofu_${TOFU_VERSION}_SHA256SUMS

ZIPFILE_NAME=tofu_${TOFU_VERSION}_linux_amd64.zip
SUMFILE_NAME=tofu_${TOFU_VERSION}_SHA256SUMS
CHECKSUM=$(sha256sum ./tmp/"${ZIPFILE_NAME}" | cut -f 1 -d ' ')
EXPECTED_CHECKSUM=$(grep "${ZIPFILE_NAME}" "./tmp/${SUMFILE_NAME}" | cut -f 1 -d ' ')

if [ "${CHECKSUM}" = "${EXPECTED_CHECKSUM}" ]; then
    echo "CHECKSUM IS OK"
else
    echo "MISMATCH" && exit 1
fi

unzip -o -d ./tmp ./tmp/tofu_${TOFU_VERSION}_linux_amd64.zip
chmod + ./tmp/tofu
cp ./tmp/tofu .

